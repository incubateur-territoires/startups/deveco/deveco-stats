# deveco stats

Lance une instance de matomo et sa base de données et l'expose derrière un proxy nginx sur le port `8080`.

source: https://github.com/matomo-org/docker/tree/master/.examples/nginx

```sh
docker-compose up
```
